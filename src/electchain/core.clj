(ns electchain.core
  (:require [ring.adapter.undertow :refer [run-undertow]]
            [clojure.edn :as edn]
            [clojure.data.json :as json]
            [compojure.core :refer :all]
            [compojure.route :as route])
  (:gen-class))

(def chain (atom {}))
(def current-transaction (atom []))

(def thingy ["moshe" "Shalom" "Timothy"])

(defn new-transaction [sender recipient amount]
  (swap! current-transaction conj {
                                   :sender sender
                                   :recipient recipient
                                   :amount amount
                                   }))

(defn new-block [proof previous-hash]
  (swap! chain conj {:index (count @chain)
    :timestamp (System/currentTimeMillis)
    :transactions @current-transaction
    :proof proof
    :previous_hash (or previous-hash (hash (last @chain)))}))


(defn post-transaction [{:keys [sender recipient amount]}]
  (do (new-transaction sender recipient amount)
      (json/write-str {:message "Created new entry"})))

(defn unwrap [req]
  (json/read-str(slurp (:body req))))

(defroutes app
  (GET "/" [] "<h1>Hello World</h1>")
;;  (GET "/user/:id" [id] (str "Hello again too " (get thingy (edn/read-string id)) " you piece of shit!"))
  (GET "/chain" [] {:body (json/write-str {:chain @chain :length (count @chain)}) :status 200})
  (POST "/transactions/new" req (post-transaction (unwrap req)))
  (route/not-found "<h1>Page not found</h1>"))

(defn -main
  [& args]
  (run-undertow app {:host "0.0.0.0" :port 8080}))
